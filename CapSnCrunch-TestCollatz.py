#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "999 999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999)
        self.assertEqual(j, 999)

    def test_read_3(self):
        s = "12345 123456\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  12345)
        self.assertEqual(j, 123456)

    def test_read_4(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,      1)
        self.assertEqual(j, 999999)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(10, 100)
        self.assertEqual(v, 119)

    def test_eval_3(self):
        v = collatz_eval(1, 100000)
        self.assertEqual(v, 351)

    def test_eval_4(self):
        v = collatz_eval(999, 9999)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 987, 98765, 351)
        self.assertEqual(w.getvalue(), "987 98765 351\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 9999, 10001, 180)
        self.assertEqual(w.getvalue(), "9999 10001 180\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("8 10\n999 1001\n111 121\n12345 98765\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8 10 20\n999 1001 143\n111 121 96\n12345 98765 351\n")

    def test_solve_3(self):
        r = StringIO("1010 101010\n500 1000\n6000 6300\n6800 7000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1010 101010 351\n500 1000 179\n6000 6300 262\n6800 7000 257\n")

    def test_solve_4(self):
        r = StringIO("3000 3500\n3500 4000\n4000 4500\n4500 5000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3000 3500 199\n3500 4000 238\n4000 4500 215\n4500 5000 210\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
